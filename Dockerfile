FROM alpine:latest

# install packages
RUN apk update && apk add --no-cache \
	bash openssh-client expect jq git rsync lftp ca-certificates

# change shell to bash (just to be safe)
RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

WORKDIR /app
CMD ["/bin/bash"]
